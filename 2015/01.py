from aoc_wrapper import BaseRiddleSolver


class RiddleSolver(BaseRiddleSolver):
    def part_one(self):
        return self.input.count("(") - self.input.count(")")

    def part_two(self):
        level = 0
        for i, n in enumerate(self.input):
            if n == "(":
                level += 1
            else:
                level -= 1
            if level == -1:
                return i + 1


RiddleSolver(1).solve()