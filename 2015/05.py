import re

from aoc_wrapper import BaseRiddleSolver


class RiddleSolver(BaseRiddleSolver):
    def part_one(self):
        def _check_vowels(st: str) -> bool:
            counter = 0
            for ch in st:
                if ch in "aeiou":
                    counter += 1
            return counter >= 3

        def _check_repeat(st: str) -> bool:
            # breakpoint()
            for idx, ch in enumerate(st[1:]):
                if ch == st[idx]:
                    return True
            return False

        def _check_no_forbidden(st: str) -> bool:
            return "ab" not in st and "cd" not in st and "pq" not in st and "xy" not in st

        lines = self.input.split("\n")[:-1]
        n_nice = 0
        for st in lines:
            if _check_vowels(st) and _check_no_forbidden(st) and _check_repeat(st):
                n_nice += 1
        return n_nice

    def part_two(self):
        def _check_double_repeat(st: str) -> bool:
            # double repeat
            return bool(re.search(r"(..).*\1", st))

        def _check_repeat(st: str) -> bool:
            # spaced repeat
            return bool(re.search(r"(.).\1", st))

        lines = self.input.split("\n")[:-1]
        n_nice = 0
        for st in lines:
            if _check_double_repeat(st) and _check_repeat(st):
                n_nice += 1
        return n_nice


RiddleSolver(5).solve()  #Adjust the day here.