import hashlib

from aoc_wrapper import BaseRiddleSolver


class RiddleSolver(BaseRiddleSolver):
    def part_one(self):
        line = self.input.strip("\n")
        i = 0
        while True:
            md5 = hashlib.md5((line + str(i)).encode()).hexdigest()
            if md5.startswith("00000"):
                return i
            i += 1

    def part_two(self):
        line = self.input.strip("\n")
        i = 0
        while True:
            md5 = hashlib.md5((line + str(i)).encode()).hexdigest()
            if md5.startswith("000000"):
                return i
            i += 1


RiddleSolver(4).solve()  #Adjust the day here.