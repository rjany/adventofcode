from aoc_wrapper import BaseRiddleSolver


class RiddleSolver(BaseRiddleSolver):
    def part_one(self):
        directions = self.input
        loc = [0,0]
        served = {tuple(loc): 1}
        for direction in directions:
            if direction == "^":
                loc[1] += 1
            elif direction == "v":
                loc[1] -= 1
            elif direction == ">":
                loc[0] += 1
            else:
                loc[0] -= 1
            if tuple(loc) not in served:
                served[tuple(loc)] = 1
            else:
                served[tuple(loc)] = served[tuple(loc)] + 1
        return len(served)

    def part_two(self):
        directions = self.input
        santa = [0,0]
        robo = [0,0]
        served = {(0,0): 2}
        for idx, direction in enumerate(directions):
            deliverer = santa if idx % 2 else robo
            if direction == "^":
                deliverer[1] += 1
            elif direction == "v":
                deliverer[1] -= 1
            elif direction == ">":
                deliverer[0] += 1
            else:
                deliverer[0] -= 1
            if tuple(deliverer) not in served:
                served[tuple(deliverer)] = 1
            else:
                served[tuple(deliverer)] = served[tuple(deliverer)] + 1
        return len(served)


RiddleSolver(3).solve()