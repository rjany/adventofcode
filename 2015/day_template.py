from aoc_wrapper import BaseRiddleSolver


class RiddleSolver(BaseRiddleSolver):
    def part_one(self):
        lines = self.input.split("\n")[:-1]
        return

    def part_two(self):
        lines = self.input.split("\n")[:-1]
        return


RiddleSolver(1).solve()  #Adjust the day here.