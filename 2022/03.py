from aoc_wrapper import BaseRiddleSolver


class RiddleSolver(BaseRiddleSolver):
    def part_one(self):
        lines = self.input.split("\n")[:-1]
        items = " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
        errors = ""
        for rs in lines:
            # breakpoint()
            comp_a = rs[:len(rs)//2]
            comp_b = rs[len(rs)//2:]
            error = list(set(comp_a).intersection(comp_b))[0]
            errors += error
        score = 0
        for c in errors:
            score += items.index(c)
        return score


    def part_two(self):
        lines = self.input.split("\n")[:-1]
        items = " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
        idents = ""
        teams = []
        for i in range(len(lines)//3):
            print(f"From {i*3} to {i*3+3}")
            teams.append(lines[i*3:i*3+3])
        for team in teams:
            idents += list(set(team[0]).intersection(team[1], team[2]))[0]
        score = 0
        for c in idents:
            score += items.index(c)
        return score


RiddleSolver(3).solve()  #Adjust the day here.
