import requests
from os import environ


class BaseRiddleSolver():
    def __init__(self, day, year=2022) -> None:
        self.day = day
        self.year = year
        self.input = self.get_input()

    def part_one(self):
        return "Not yet implemented."

    def part_two(self):
        return "Not yet implemented."

    def get_input(self):
        response = requests.get(f"https://adventofcode.com/{self.year}/day/{self.day}/input",
            cookies={"session": environ.get("AOC_SESSION")})
        return response.text

    def solve(self):
        print(f"Answer Part 1: {self.part_one()}")
        print(f"Answer Part 2: {self.part_two()}")
