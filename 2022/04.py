from aoc_wrapper import BaseRiddleSolver


class RiddleSolver(BaseRiddleSolver):
    def part_one(self):
        lines = self.input.split("\n")[:-1]
        counter = 0
        for assignment in lines:
            elf_a = [int(i) for i in assignment.split(",")[0].split("-")]
            elf_b = [int(i) for i in assignment.split(",")[1].split("-")]
            if (elf_a[0] >= elf_b[0] and elf_a[1] <= elf_b[1]) or (elf_b[0] >= elf_a[0] and elf_b[1] <= elf_a[1]):
                counter += 1        
        return counter

    def part_two(self):
        lines = self.input.split("\n")[:-1]
        counter = 0
        for assignment in lines:
            elf_a = [int(i) for i in assignment.split(",")[0].split("-")]
            elf_b = [int(i) for i in assignment.split(",")[1].split("-")]
            if len(set(range(elf_a[0], elf_a[1] + 1)).intersection(range(elf_b[0], elf_b[1] + 1))) > 0:
                counter += 1        
        return counter


RiddleSolver(4).solve()  #Adjust the day here.
