from queue import Queue
from aoc_wrapper import BaseRiddleSolver


class RiddleSolver(BaseRiddleSolver):
    def part_one(self):
        line = self.input.split("\n")[:-1][0]
        q = Queue(maxsize=4)
        for i, ch in enumerate(line):
            q.put(ch)
            print(f"Index: {i}, {list(q.queue)}")
            if len(set(q.queue)) == 4:
                return i + 1
            if q.full():
                q.get()

    def part_two(self):
        line = self.input.split("\n")[:-1][0]
        q = Queue(maxsize=14)
        for i, ch in enumerate(line):
            q.put(ch)
            print(f"Index: {i}, {list(q.queue)}")
            if len(set(q.queue)) == 14:
                return i + 1
            if q.full():
                q.get()

RiddleSolver(6).solve()  #Adjust the day here.
