from aoc_wrapper import BaseRiddleSolver


class RiddleSolver(BaseRiddleSolver):
    def part_one(self):
        lines = self.input.split("\n")[:-1]
        games = [i.replace(" ", "") for i in lines]
        scores = { 
            "AX": 1+3,
            "AY": 2+6,
            "AZ": 3+0,
            "BX": 1+0,
            "BY": 2+3,
            "BZ": 3+6,
            "CX": 1+6,
            "CY": 2+0,
            "CZ": 3+3}
        score = 0
        for _i in games:
            score += scores[_i]
        return score

    def part_two(self):
        lines = self.input.split("\n")[:-1]
        games = [i.replace(" ", "") for i in lines]
        scores = { 
            "AA": 1+3,
            "AB": 2+6,
            "AC": 3+0,
            "BA": 1+0,
            "BB": 2+3,
            "BC": 3+6,
            "CA": 1+6,
            "CB": 2+0,
            "CC": 3+3}

        play = {
            "AX": "AC",
            "AY": "AA",
            "AZ": "AB",
            "BX": "BA",
            "BY": "BB",
            "BZ": "BC",
            "CX": "CB",
            "CY": "CC",
            "CZ": "CA",
        }
        score = 0
        for _i in games:
            score += scores[play[_i]]
        return score


RiddleSolver(2).solve()  #Adjust the day here.
