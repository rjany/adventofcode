from aoc_wrapper import BaseRiddleSolver


class RiddleSolver(BaseRiddleSolver):

    def __init__(self, day) -> None:
        super().__init__(day)
        self.brackets = ["()", "[]", "{}", "<>"]
        self.closing_brackets = [")", "}", ">", "]"]
        self.score_map_1 = {")": 3, "]": 57, "}": 1197, ">": 25137}
        self.score_map_2 = {")": 1, "]": 2, "}": 3, ">": 4}
        self.bracket_map = {"(": ")", "{": "}", "[": "]", "<": ">"}

    def _is_incomplete(self, line):
        while any(x in line for x in self.brackets):
            for br in self.brackets:
                line = line.replace(br, "")
        for br in self.closing_brackets:
            if br in line:
                return (line, False)
        return (line, True)

    def part_one(self):
        score = 0
        for line in self.input:
            remainder, is_incomplete = self._is_incomplete(line)
            if not is_incomplete:
                for ch in remainder:
                    if ch in self.closing_brackets:
                        score += self.score_map_1[ch]
                        break
        return score

    def part_two(self):
        scores = []
        for line in self.input:
            score = 0
            remainder, is_incomplete = self._is_incomplete(line)
            if is_incomplete:
                missing_brackets = ""
                for ch in remainder[::-1]:
                    score *= 5
                    score += self.score_map_2[self.bracket_map[ch]]
                    missing_brackets += self.bracket_map[ch]
            scores.append(score)
        scores = [score for score in scores if score is not 0]
        scores.sort()
        return scores[round(len(scores)/2)]

RiddleSolver(10).solve()
