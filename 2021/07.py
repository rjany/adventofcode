from aoc_wrapper import BaseRiddleSolver


class RiddleSolver(BaseRiddleSolver):

    def __init__(self, day) -> None:
        super().__init__(day)
        self.input = [int(i) for i in self.input[0].split(",")]

    def find_min_consumption(self, function):
        target = min(self.input)
        fuel_cons = function(self.input, target)
        prev_fuel_cons = fuel_cons + 1 # Initial value must be bigger than fuel_cons
        counter = 1
        while fuel_cons < prev_fuel_cons:
            prev_fuel_cons = fuel_cons
            fuel_cons = function(self.input, target + counter)
            counter += 1
        return prev_fuel_cons

    def part_one(self):
        def fuel_consumption(input, target):
            return sum(abs(val - target) for val in input)
        return self.find_min_consumption(fuel_consumption)

    def part_two(self):
        def fuel_consumption(input, target):
            return sum(sum(range(1, abs(val - target) + 1)) for val in input)
        return self.find_min_consumption(fuel_consumption)


RiddleSolver(7).solve()
