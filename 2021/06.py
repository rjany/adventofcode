from aoc_wrapper import BaseRiddleSolver


class RiddleSolver(BaseRiddleSolver):
    def part_one(self):
        # Leaving this here to show where I started...
        days = 80
        list_fish = [int(fish) for fish in self.input[0].split(",")]
        for _ in range(days):
            print(len(list_fish))
            list_fish.extend(list_fish.count(0) * [9])
            list_fish = [7 if n==0 else n for n in list_fish]
            list_fish = [n -1 for n in list_fish]
        return len(list_fish)

    def part_two(self):
        days = 256
        list_fish = [int(fish) for fish in self.input[0].split(",")]
        simple_fish = [0, 0, 0, 0, 0, 0, 0, 0, 0]
        for fish in list_fish:
            simple_fish[fish] += 1
        for _ in range(days):
            baby_fish = simple_fish[0]
            for i in range(8):
                simple_fish[i] = simple_fish[i+1]
            simple_fish[8] = baby_fish
            simple_fish[6] += baby_fish # mama fish actually
        return sum(simple_fish)


RiddleSolver(6).solve()
