from aoc_wrapper import BaseRiddleSolver


class RiddleSolver(BaseRiddleSolver):
    def part_one(self):
        gamma , epsilon= ("", "")
        for i in range(12):
            temp = [int(line[i]) for line in self.input]
            gamma += "1" if sum(temp) > len(temp)/2 else "0"
            epsilon += ["1", "0"][int(gamma[i])]
        return int(gamma, 2) * int(epsilon, 2)

    def part_two(self):
        remainder_oxy = self.input
        remainder_co2 = self.input
        for i in range(12):
            if len(remainder_oxy) != 1:
                temp_oxy = [int(line[i]) for line in remainder_oxy]
                bit_criteria_oxy = "1" if sum(temp_oxy) >= len(temp_oxy)/2 else "0"
                remainder_oxy = [number for number in remainder_oxy if number[i] == bit_criteria_oxy]
            if len(remainder_co2) != 1:
                temp_co2 = [int(line[i]) for line in remainder_co2]
                bit_criteria_co2 = "1" if sum(temp_co2) >= len(temp_co2)/2 else "0"
                remainder_co2a = [number for number in remainder_co2 if number[i] == bit_criteria_co2]
                remainder_co2 = [number for number in remainder_co2 if number[i] != bit_criteria_co2]
                if not remainder_co2:
                    remainder_co2 = remainder_co2a
        oxygen = remainder_oxy[0]
        co2 = remainder_co2[0]
        return int(oxygen, 2) * int(co2, 2)

RiddleSolver(3).solve()
