from aoc_wrapper import BaseRiddleSolver


def build_input(input):
    drawn_nums = [int(n) for n in input[0].split(",")]
    boards = [
        [
            [[int(i), False] for i in row.split(" ") if i]
            for row in input[idx : idx + 5]
        ]
        for idx in range(2, len(input), 6)
    ]
    return drawn_nums, boards


def count_unsolved_boards(boards):
        return len(boards) - sum(1 for board in boards if board.solved)


class Board():
    def __init__(self, board) -> None:
        self.board = board
        self.solved = False
        self.last_drawn_num = None

    def print_board(self):
        for row in self.board:
            print(
                f"{str(row[0][0]).rjust(4, ' ' ) + ('x' if row[0][1] else ' ')} "\
                f"{str(row[1][0]).rjust(4, ' ' ) + ('x' if row[1][1] else ' ')} "\
                f"{str(row[2][0]).rjust(4, ' ' ) + ('x' if row[2][1] else ' ')} "\
                f"{str(row[3][0]).rjust(4, ' ' ) + ('x' if row[3][1] else ' ')} "\
                f"{str(row[4][0]).rjust(4, ' ' ) + ('x' if row[4][1] else ' ')}")

    def mark_number(self, drawn_num):
        self.last_drawn_num = drawn_num
        for row in self.board:
            for num in row:
                if num[0] == self.last_drawn_num:
                    num[1] = True

    def check_winner(self):
        if (
            # self._check_diagonal_down_winner() or
            # self._check_diagonal_up_winner() or
            self._check_col_winner(range(5)) or
            self._check_row_winner()):
            self.solved = True

    def calculate_score_board(self):
        return self.last_drawn_num * sum(
            sum(row[idx][0] for idx in range(5) if not row[idx][1])
            for row in self.board
        )

    # Took me hours to read that diagonals don't count. :(
    def _check_diagonal_down_winner(self):
        return sum(self.board[idx][idx][1] for idx in range(5)) == 5

    def _check_diagonal_up_winner(self):
        return sum(self.board[idx][4-idx][1] for idx in range(5)) == 5

    def _check_row_winner(self):
        for row in self.board:
            if sum(m[1] for m in row) == 5:
                return True

    def _check_col_winner(self, in_range):
        for col in range(len(self.board)):
            if sum(self.board[row][col][1] for row in in_range) == 5:
                return True


class RiddleSolver(BaseRiddleSolver):
    def part_one(self):
        drawn_nums, raw_boards  = build_input(self.input)
        boards = [Board(raw_board) for raw_board in raw_boards]
        for drawn_num in drawn_nums:
            for board in boards:
                board.mark_number(drawn_num)
                board.check_winner()
                if board.solved == True:
                    return board.calculate_score_board()


    def part_two(self):
        drawn_nums, raw_boards  = build_input(self.input)
        boards = [Board(raw_board) for raw_board in raw_boards]
        for drawn_num in drawn_nums:
            for board in boards:
                if not board.solved:
                    board.mark_number(drawn_num)
                    board.check_winner()
                    if count_unsolved_boards(boards) == 0:
                        return board.calculate_score_board()


RiddleSolver(4).solve()