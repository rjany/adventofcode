from aoc_wrapper import BaseRiddleSolver


class RiddleSolver(BaseRiddleSolver):

    def __init__(self, day) -> None:
        super().__init__(day)

        # self.input = [
        #     "999999999999",
        #     "921999432109",
        #     "939878949219",
        #     "998567898929",
        #     "987678967899",
        #     "998999656789",
        #     "999999999999"
        # ]


    def part_one(self):
        sum_risk_lvl = 0
        for y, line in enumerate(self.input):
            out = ""
            for x in range(len(line)):
                idx = (x,y)
                if self._is_low(idx):
                    out += "-"
                    sum_risk_lvl += int(self.input[y][x]) + 1
                elif self._is_high(idx):
                    out += "X"
                else:
                    out += "-"
            print(out)
        return sum_risk_lvl

    def _is_low(self, idx):
        x, y = (idx[0], idx[1])
        for nb_hor in [-1, 1]:
            x_nb = int(x) + nb_hor
            if x_nb < 0:
                nb_val = None
            else:
                try:
                    nb_val = self.input[y][x_nb]
                except IndexError:
                    nb_val = None
            if not nb_val or nb_val > self.input[y][x]:
                continue
            else:
                return False
        for nb_ver in [-1, 1]:
            y_nb = int(y) + nb_ver
            if y_nb < 0:
                nb_val = 0
            else:
                try:
                    nb_val = self.input[y_nb][x]
                except IndexError:
                    nb_val = None
            if not nb_val or nb_val > self.input[y][x]:
                continue
            else:
                return False
        return True

    def _is_high(self, idx):
        x, y = (idx[0], idx[1])
        for nb_hor in [-1, 1]:
            x_nb = int(x) + nb_hor
            if x_nb < 0:
                nb_val = None
            else:
                try:
                    nb_val = self.input[y][x_nb]
                except IndexError:
                    nb_val = None
            if not nb_val or nb_val <= self.input[y][x]:
                continue
            else:
                return False
        for nb_ver in [-1, 1]:
            y_nb = int(y) + nb_ver
            if y_nb < 0:
                nb_val = 0
            else:
                try:
                    nb_val = self.input[y_nb][x]
                except IndexError:
                    nb_val = None
            if not nb_val or nb_val <= self.input[y][x]:
                continue
            else:
                return False
        return True

    # def part_two(self):
    #     low_points = []
    #     for y, line in enumerate(self.input):
    #         for x in range(len(line)):
    #             idx = (x,y)
    #             if self._is_low(idx):
    #                 low_points.append(idx)
    #     print(low_points)


RiddleSolver(9).solve()
