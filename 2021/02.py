from aoc_wrapper import BaseRiddleSolver


class RiddleSolver(BaseRiddleSolver):
    def part_one(self):
        instructions = [(inst.split(" ")[0], int(inst.split(" ")[1])) for inst in self.input]
        location = {"forward": 0, "down": 0, "up": 0}
        for i in instructions:
            location[i[0]] += i[1]
        return location["forward"] * (- location["up"] + location["down"])

    def part_two(self):
        instructions = [(inst.split(" ")[0], int(inst.split(" ")[1])) for inst in self.input]
        location = {"horizontal": 0, "depth": 0, "aim": 0}
        for i in instructions:
            if i[0] == "down":
                location["aim"] += i[1]
            elif i[0] == "up":
                location["aim"] -= i[1]
            else:
                location["horizontal"] += i[1]
                location["depth"] += location["aim"] * i[1]
        return location["horizontal"] * location["depth"]

RiddleSolver(2).solve()
