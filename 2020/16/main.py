import re


def read_file(input_file):
    with open(input_file) as file:
        content = file.readlines()
    rules = []
    own_ticket = []
    other_tickets = []
    mapping = {0: rules, 1: own_ticket, 2: other_tickets}
    section = 0
    for line in content:
        if line == "\n":
            section += 1
            continue
        else:
            mapping[section].append(line.strip())
    own_ticket = [int(num) for num in own_ticket[1].split(",")]
    other_tickets = [ticket.split(",") for ticket in other_tickets[1:]]
    return rules, own_ticket, other_tickets


def parse_rules(raw_rules):
    rules = []
    ranged_rules = []
    for rule in raw_rules:
        rules.append(re.findall(r"(\d+-\d+)", rule))
    for rule in rules:
        lower_range = [int(i) for i in rule[0].split("-")]
        upper_range = [int(i) for i in rule[1].split("-")]
        range_1 = range(lower_range[0], lower_range[1] + 1)
        range_2 = range(upper_range[0], upper_range[1] + 1)
        ranged_rules.append((range_1, range_2))
    return rules, ranged_rules


def part_one(input_file):
    rules, own_ticket, other_tickets = read_file(input_file)
    rules, _ = parse_rules(rules)
    invalid_nums = []
    for ticket in other_tickets:
        for num in ticket:
            rule_found = False
            num = int(num)
            for rule in rules:
                lower_range = [int(i) for i in rule[0].split("-")]
                upper_range = [int(i) for i in rule[1].split("-")]
                if num in range(lower_range[0], lower_range[1] + 1) or num in range(upper_range[0], upper_range[1] + 1):
                    rule_found = True
                    break
            if not rule_found:
                invalid_nums.append(num)
                break
    return sum(invalid_nums)


def solve_mapping(mapping):
    mapping = list(mapping.items())
    while mapping:
        for index, r_map in enumerate(mapping):
            if len(r_map[1]) == 1:
                mapping.pop(index)
                for _, options in mapping:
                    options.remove(r_map[1][0])


def part_two(input_file):
    rules, own_ticket, other_tickets = read_file(input_file)
    _, rules = parse_rules(rules)
    valid_tickets = []
    for ticket in other_tickets:
        for num in ticket:
            rule_found = False
            num = int(num)
            for range_1, range_2 in rules:
                if num in range_1 or num in range_2:
                    rule_found = True
                    break
            if not rule_found:
                break
        if rule_found:
            valid_tickets.append(ticket)
    # Get mapping done
    # Key: Rule Num ; Val: Ticket Row
    mapping = {}
    for idx_rule, rule in enumerate(rules):
        mapping[idx_rule] = []
        for idx_ticket in range(len(rules)):
            row = {int(ticket[idx_ticket]) for ticket in valid_tickets}
            valid_nums = set(rule[0]).union(set(rule[1]))
            if row.issubset(valid_nums):
                mapping[idx_rule].append(idx_ticket)
                # break
    solve_mapping(mapping)
    # Apply to own ticket
    # First 6 rules begin with 'departure'
    res = 1
    for idx in range(0, 6):
        res *= own_ticket[mapping[idx][0]]
    return res


if __name__ == '__main__':
    # print(part_one(input_file="input.txt"))
    print(part_two(input_file="input.txt"))
