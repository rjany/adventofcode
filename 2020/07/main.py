import re


def read_file(input_file):
    with open(input_file) as file:
        content = file.readlines()
    return [line.strip() for line in content]


def find_bags_that_may_contain_color(raw_input, bag_color):
    bags = []
    lines = [line for line in raw_input if re.search(rf"(?<!^){bag_color}", line)]
    if not lines:
        return []
    for line in lines:
        bag_color = " ".join(line.split()[:2])
        bags.append(bag_color)
        bags = bags + find_bags_that_may_contain_color(raw_input, bag_color)
    return bags


def part_one(input_file, bag_color):
    raw_input = read_file(input_file)
    return len(set(find_bags_that_may_contain_color(raw_input, bag_color)))


# Part 2
def find_amount_of_bags(raw_input, bag_color):
    bag_count = 0
    lines = [line for line in raw_input if re.search(rf"^{bag_color}", line)]
    if not lines:
        return bag_count
    for line in lines:
        if "contain no other bags" in line:
            continue
        # Only works if it's a single digit amount of bags. Which it is in the input :)
        sub_bags = [(int(sub_bag.strip(" .bags")[0]), sub_bag.strip(" .bags")[2:]) for sub_bag in
                    line.split("contain")[1].split(",")]
        for sub_bag in sub_bags:
            bag_count += sub_bag[0] + sub_bag[0] * find_amount_of_bags(raw_input, sub_bag[1])
    return bag_count


def part_two(input_file, bag_color):
    raw_input = read_file(input_file)
    return find_amount_of_bags(raw_input, bag_color)


if __name__ == '__main__':
    print(part_one(input_file="input.txt", bag_color="shiny gold"))
    print(part_two(input_file="input.txt", bag_color="shiny gold"))
