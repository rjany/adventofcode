from collections import deque


def read_file(input_file):
    with open(input_file) as file:
        content = file.readline()
    return [int(i) for i in content.strip().split(",")]


def game_simulator(inp, duration):
    spoken_nums = {0: deque([], 2)}
    turn = 0
    last_spoken = 0
    for spoken_num in inp:
        turn += 1
        spoken_nums[spoken_num] = deque([turn, ], 2)
        last_spoken = spoken_num
    while turn < duration:
        print(turn)
        turn += 1
        if len(spoken_nums[last_spoken]) == 1:
            spoken_nums[0].append(turn)
            last_spoken = 0
        else:
            last_spoken = spoken_nums[last_spoken][1] - spoken_nums[last_spoken][0]
            if spoken_nums.get(last_spoken):
                spoken_nums[last_spoken].append(turn)
            else:
                spoken_nums[last_spoken] = deque([turn], 2)
    return last_spoken


def run(input_file, duration):
    inp = read_file(input_file)
    spoken_nums = game_simulator(inp, duration)
    return spoken_nums


if __name__ == '__main__':
    # print(run(input_file="input.txt", duration=2020))
    print(run(input_file="input.txt", duration=30000000))
