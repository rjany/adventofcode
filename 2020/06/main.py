def read_file(input_file):
    with open(input_file) as file:
        content = file.readlines()
    return [line.strip() for line in content]


def group_answers(raw_input):
    list_of_groups = []
    group = []
    for line in raw_input:
        if not line:
            list_of_groups.append(group)
            group = []
        group.append(line.split())
    # Add last group
    list_of_groups.append(group)

    return list_of_groups


def flatten_group_answers(list_of_groups):
    flat_groups = []
    for group in list_of_groups:
        group = "".join([answer for line in group for answer in line])
        flat_groups.append(group)
    return flat_groups


def part_one(input_file):
    raw_input = read_file(input_file)
    flat_groups = flatten_group_answers(group_answers(raw_input))
    return sum([len(set(group)) for group in flat_groups])


def part_two(input_file):
    raw_input = read_file(input_file)
    groups = group_answers(raw_input)
    all_yes_questions = 0
    for raw_group in groups:
        group = [group[0] for group in raw_group if group != []]
        target_count = len(group)
        for answer in group[0]:
            count = 0
            for participant in group:
                if answer in participant:
                    count += 1
            if count == target_count:
                all_yes_questions += 1
    return all_yes_questions


if __name__ == '__main__':
    print(part_one(input_file="input.txt"))
    print(part_two(input_file="input.txt"))
