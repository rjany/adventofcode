import re


def read_raw_data(input_file):
    with open(input_file) as file:
        content = file.readlines()
    return parse_passports(content)


def pass_list_to_dict(passport):
    passport = [item for line in passport for item in line]
    pass_dict = dict(item.split(":") for item in passport)
    return pass_dict


def parse_passports(raw_input):
    list_of_passports = []
    passport = []
    for line in raw_input:
        line = line.strip()
        if line == "":
            list_of_passports.append(pass_list_to_dict(passport))
            passport = []
        passport.append(line.split())
    # Add last passport
    list_of_passports.append(pass_list_to_dict(passport))
    return list_of_passports


# Part 1


def validate_passport(pass_dict):
    req_passport_fields = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid", ]
    for field in req_passport_fields:
        if not pass_dict.get(field):
            return False
    return True


def part_one(input_file):
    valid_passports = []
    invalid_passports = []
    list_of_passports = read_raw_data(input_file)

    for pass_dict in list_of_passports:
        if validate_passport(pass_dict):
            valid_passports.append(pass_dict)
        else:
            invalid_passports.append(pass_dict)
    return len(valid_passports)


# Part 2

def check_byr(value):
    # byt: four digits, 1920-2002
    if re.search(r"\d{4}", value):
        if 1919 < int(value) < 2003:
            return True
    return False


def check_iyr(value):
    # iyr: four digits, 2010-2020
    if re.search(r"\d{4}", value):
        int(value)
        if 2009 < int(value) < 2021:
            return True
    return False


def check_eyr(value):
    # eyr: four digits, >2020, <2030
    if re.search(r"\d{4}", value):
        int(value)
        if 2019 < int(value) < 2031:
            return True
    return False


def check_hgt(value):
    # hgt: 150-193cm or 59-76in
    unit = value[-2:]
    if unit == "cm":
        if re.search(r"\d{3}", value[:-2]):
            if 149 < int(value[:-2]) < 194:
                return True
    if unit == "in":
        if re.search(r"\d{2}", value[:-2]):
            if 58 < int(value[:-2]) < 77:
                return True
    return False


def check_hcl(value):
    # hcl: # followed by 6 chars 0-9 or a-f
    if re.search(r"#[0-9a-f]{6}", value):
        return True
    return False


def check_ecl(value):
    # ecl: amb, blu, brn, gry, grn, hzl, oth
    if re.search(r"(amb|blu|brn|gry|grn|hzl|oth)", value):
        return True
    return False


def check_pid(value):
    # pid: nine digit number incl. leading zeros
    if re.search(r"^[0-9]{9}$", value):
        return True
    return False


def validate_passport_part_2(pass_dict):
    req_passport_fields = [("byr", check_byr),
                           ("iyr", check_iyr),
                           ("eyr", check_eyr),
                           ("hgt", check_hgt),
                           ("hcl", check_hcl),
                           ("ecl", check_ecl),
                           ("pid", check_pid)]
    for key, function in req_passport_fields:
        if not pass_dict.get(key):
            return False
        if not function(pass_dict[key]):
            return False
    return True


def part_two(input_file):
    valid_passports = []
    invalid_passports = []
    list_of_passports = read_raw_data(input_file)

    for pass_dict in list_of_passports:
        if validate_passport_part_2(pass_dict):
            valid_passports.append(pass_dict)
        else:
            invalid_passports.append(pass_dict)
    return len(valid_passports)


if __name__ == '__main__':
    print(part_one(input_file="input.txt"))
    print(part_two(input_file="input.txt"))
