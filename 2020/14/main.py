def read_file(input_file):
    with open(input_file) as file:
        content = file.readlines()
    return [line.strip() for line in content]


def apply_mask(mask, mem_val):
    mem_val = bin(int(mem_val))[2:].zfill(36)
    ret = ""
    for i in range(0, 36):
        if mask[i] == "X":
            ret += mem_val[i]
        else:
            ret += mask[i]
    return ret


def part_one(input_file):
    content = read_file(input_file)
    mem = {}
    mask = ""
    for line in content:
        if line[:4] == "mask":
            mask = line.split("=")[1].strip()
        else:
            mem_name = line.split("=")[0].strip("mem[] ")
            mem_val = line.split("=")[1].strip()
            mem[mem_name] = int(apply_mask(mask, mem_val), 2)
    return sum(mem.values())


if __name__ == '__main__':
    print(part_one(input_file="input.txt"))
    # print(part_two(input_file="input.txt"))
