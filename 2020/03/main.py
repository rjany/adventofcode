def part_one():
    with open("input.txt") as file:
        # Perform first down movement
        file.readline()
        counter = 0
        x_coor = 3
        for line in file:
            layout = line.strip()
            if layout[x_coor] == "#":
                counter += 1
            x_coor += 3
            if x_coor >= len(layout):
                x_coor = x_coor - len(layout)
        return counter


def part_two(right, down):
    with open("input.txt") as file:
        # Perform first down movement
        for i in range(down):
            file.readline()
        counter = 0
        x_coor = right
        for line in file:
            layout = line.strip()
            if layout[x_coor] == "#":
                counter += 1
            # Set new x_coor
            x_coor += right
            if x_coor >= len(layout):
                x_coor = x_coor - len(layout)
            # Perform extra down movement(s)
            for i in range(down - 1):
                file.readline()
        return counter


def result_part_two():
    return part_two(1, 1) * part_two(3, 1) * part_two(5, 1) * part_two(7, 1) * part_two(1, 2)


if __name__ == '__main__':
    print(part_one())
    print(result_part_two())

