def read_file(input_file):
    with open(input_file) as file:
        content = file.readlines()
    return [line.strip() for line in content]


def part_one(input_file):
    inp = [int(joltage) for joltage in read_file(input_file)]
    inp.sort()
    diffs = [inp[0], ]
    for i in range(0, len(inp) - 1):
        diffs.append(inp[i + 1] - inp[i])
    diffs.append(3)
    ones = diffs.count(1)
    threes = diffs.count(3)
    return ones * threes


# Part 2
# That's legacy code that proved to be useful still... Will be refactored once I find the nerve to
# look at it again. Currently used to help determine what numbers are essential and optional.
def options_checker(inp_val, next_inp):
    options = 0
    if inp_val + 3 in next_inp:
        options += 1
    if inp_val + 2 in next_inp:
        options += 1
    if inp_val + 1 in next_inp:
        options += 1
    return options


def get_optionals(inp, options):
    optionals = []
    for i in range(len(options)):
        if options[i] != 1:
            optionals.append(inp[i])
    return optionals


def group_optionals(optionals):
    # [5, 6, 11]
    grouped_optionals = []
    sublist = []
    while optionals:
        first = optionals.pop(0)
        if not sublist:
            sublist.append(first)
        elif first == sublist[-1] + 1:
            sublist.append(first)
        else:
            grouped_optionals.append(sublist)
            sublist = [first, ]
    grouped_optionals.append(sublist)
    return grouped_optionals


def part_two(input_file):
    inp = [int(joltage) for joltage in read_file(input_file)]
    inp.sort()
    # First options from outlet.
    list_options = [options_checker(0, inp[0:3]), ]
    for index in range(0, len(inp) - 1):
        list_options.append(options_checker(inp[index], inp[index + 1:]))
    optionals = get_optionals(inp, list_options)
    grouped_optionals = group_optionals(optionals)
    counter = 1
    for sublist in grouped_optionals:
        if len(sublist) == 1:
            counter *= 2
        elif len(sublist) == 2:
            counter *= 4
        elif len(sublist) == 3:
            counter *= 7
        elif len(sublist) == 4:
            counter *= 13
    return counter


if __name__ == '__main__':
    print(part_one(input_file="input.txt"))
    print(part_two(input_file="input.txt"))
