def part_one():
    with open(file="input.txt") as file:
        lines = file.readlines()
        passwords = [
            (tuple(line.split()[0].split("-")), line.split()[1].strip(":"), line.split()[2]) for
            line in lines]
    counter = 0
    for passset in passwords:
        min = int(passset[0][0])
        max = int(passset[0][1])
        letter = passset[1]
        passcode = passset[2]
        if min <= passcode.count(letter) <= max:
            counter += 1
    return counter


def part_two():
    with open(file="input.txt") as file:
        lines = file.readlines()
        passwords = [
            (tuple(line.split()[0].split("-")), line.split()[1].strip(":"), line.split()[2]) for
            line in lines]
    counter = 0
    for passset in passwords:
        pos_one = int(passset[0][0]) - 1
        pos_two = int(passset[0][1]) - 1
        letter = passset[1]
        passcode = passset[2]
        if (passcode[pos_one] == letter) ^ (passcode[pos_two] == letter):
            counter += 1
    return counter


if __name__ == '__main__':
    print(part_one())
    print(part_two())
