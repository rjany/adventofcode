def read_file(input_file):
    with open(input_file) as file:
        content = file.readlines()
    return [line.strip() for line in content]


class ShipOne:
    def __init__(self):
        self.direction = self.east
        self.y_coordinate = 0
        self.x_coordinate = 0
        self.instruction_mapping = {"N": self.north,
                                    "S": self.south,
                                    "E": self.east,
                                    "W": self.west,
                                    "L": self.turn_left,
                                    "R": self.turn_right,
                                    "F": self.forward}

    def perform_maneuver(self, instruction):
        fun = self.instruction_mapping[instruction[0]]
        fun(int(instruction[1:]))

    def north(self, distance):
        self.y_coordinate += distance

    def south(self, distance):
        self.y_coordinate -= distance

    def east(self, distance):
        self.x_coordinate -= distance

    def west(self, distance):
        self.x_coordinate += distance

    def forward(self, distance):
        self.direction(distance)

    def turn_left(self, degree):
        directions = [self.north, self.west, self.south, self.east] * 2
        turn_by = int(degree/90)
        current_direction_index = directions.index(self.direction)
        self.direction = directions[current_direction_index + turn_by]

    def turn_right(self, degree):
        directions = [self.north, self.east, self.south, self.west] * 2
        turn_by = int(degree / 90)
        current_direction_index = directions.index(self.direction)
        self.direction = directions[current_direction_index + turn_by]

    def distance_travelled(self):
        return abs(self.x_coordinate) + abs(self.y_coordinate)


def part_one(input_file):
    ship = ShipOne()
    instructions = read_file(input_file)
    for instruction in instructions:
        ship.perform_maneuver(instruction)
    return ship.distance_travelled()


class ShipTwo:
    def __init__(self):
        self.waypoint = {"north": 1, "east": 10, "south": 0, "west": 0}
        self.y_coordinate = 0
        self.x_coordinate = 0
        self.instruction_mapping = {"N": self.north,
                                    "S": self.south,
                                    "E": self.east,
                                    "W": self.west,
                                    "L": self.turn_left,
                                    "R": self.turn_right,
                                    "F": self.forward}

    def perform_maneuver(self, instruction):
        fun = self.instruction_mapping[instruction[0]]
        fun(int(instruction[1:]))

    def north(self, distance):
        self.waypoint["north"] += distance

    def south(self, distance):
        self.waypoint["south"] += distance

    def east(self, distance):
        self.waypoint["east"] += distance

    def west(self, distance):
        self.waypoint["west"] += distance

    def forward(self, times):
        for _ in range(0, times):
            self.x_coordinate += self.waypoint["west"] - self.waypoint["east"]
            self.y_coordinate += self.waypoint["north"] - self.waypoint["south"]

    def turn_right(self, degree):
        turn_times = int(degree / 90)
        for _ in range(0, turn_times):
            new_north = self.waypoint["west"]
            new_east = self.waypoint["north"]
            new_south = self.waypoint["east"]
            new_west = self.waypoint["south"]
            self.waypoint = {"north": new_north, "east": new_east,
                             "south": new_south, "west": new_west}
            pass

    def turn_left(self, degree):
        turn_times = int(degree / 90)
        for _ in range(0, turn_times):
            new_north = self.waypoint["east"]
            new_east = self.waypoint["south"]
            new_south = self.waypoint["west"]
            new_west = self.waypoint["north"]
            self.waypoint = {"north": new_north, "east": new_east,
                             "south": new_south, "west": new_west}

    def distance_travelled(self):
        return abs(self.x_coordinate) + abs(self.y_coordinate)


def part_two(input_file):
    ship = ShipTwo()
    instructions = read_file(input_file)
    for instruction in instructions:
        ship.perform_maneuver(instruction)
    return ship.distance_travelled()


if __name__ == '__main__':
    print(part_one(input_file="input.txt"))
    print(part_two(input_file="input.txt"))
