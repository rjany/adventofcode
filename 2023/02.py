from aoc_wrapper import BaseRiddleSolver


class RiddleSolver(BaseRiddleSolver):
    def part_one(self):
        lines = self.input.split("\n")[:-1]
        result = 0
        limits = {"red": 12, "green": 13, "blue": 14}

        for line in lines:
            valid = True
            raw_id, raw_game = line.split(": ")
            game_id = int(raw_id.strip("Game "))
            pulls = raw_game.split("; ")
            for pull in pulls:
                pull = pull.split(", ")
                for colors in pull:
                    count, color = colors.split(" ")
                    if int(count) > limits[color]:
                        valid = False
                        break
                if not valid:
                    break
            if valid:
                result += game_id
        return result

    def part_two(self):
        lines = self.input.split("\n")[:-1]
        result = 0
        for line in lines:
            max_amounts = {"red": 0, "green": 0, "blue": 0}
            _, raw_game = line.split(": ")
            pulls = raw_game.split("; ")
            for pull in pulls:
                pull = pull.split(", ")
                for colors in pull:
                    count, color = colors.split(" ")
                    if max_amounts[color] < int(count):
                        max_amounts[color] = int(count)
            result += max_amounts["blue"] * max_amounts["green"] * max_amounts["red"]
        return result

RiddleSolver(2).solve()  #Adjust the day here.
