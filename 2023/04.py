from aoc_wrapper import BaseRiddleSolver


class RiddleSolver(BaseRiddleSolver):
    def part_one(self):
        lines = self.input.split("\n")[:-1]
        score = 0
        for line in lines:
            value = 0
            counter = -1
            _, numbers = line.split(": ")
            winning, actual = numbers.split("| ")
            winning = [x for x in winning.split(" ") if x]
            actual = [x for x in actual.split(" ") if x]
            for i in winning:
                if i in actual:
                    counter += 1
            if counter > -1:
                value = 2**counter
                score += value
        return score

    def part_two(self):
        lines = self.input.split("\n")[:-1]
        cards = {}
        for line in lines:
            id, numbers = line.split(": ")
            id = int(id.split(" ")[-1])
            winning, actual = numbers.split("| ")
            winning = [int(x) for x in winning.split(" ") if x]
            actual = [int(x) for x in actual.split(" ") if x]
            # drawn, winning, counter
            cards[id] = [winning, actual, 1]
        n = 1
        score = 0
        while True:
            for _ in range(cards[n][2]):
                counter = 0
                # For drawn number
                for i in cards[n][0]:
                    # Check in winning
                    if i in cards[n][1]:
                        counter += 1
                    # Increase draw counters
                for k in range(counter):
                    cards[n+k+1][2] += 1
            score += cards[n][2]
            n += 1
            if n > len(cards):
                break
        return score

RiddleSolver(4).solve()  #Adjust the day here.
