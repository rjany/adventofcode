import re

from aoc_wrapper import BaseRiddleSolver


class RiddleSolver(BaseRiddleSolver):
    def part_one(self):
        lines = self.input.split("\n")[:-1]
        extract = [re.findall("[0-9]", i) for i in lines]
        result = 0
        for i in extract:
            result += int("".join([i[0],i[-1]]))
        return result

    def part_two(self):
#         self.input = """two1nine
# eightwothree
# abcone2threexyz
# xtwone3four
# 4nineeightseven2
# zoneight234
# 7pqrstsixteen"""
        lines = self.input.split("\n")[:-1]
        mapping = [
            ("one", "1"),
            ("two", "2"),
            ("three", "3"),
            ("four", "4"),
            ("five", "5"),
            ("six", "6"),
            ("seven", "7"),
            ("eight", "8"),
            ("nine", "9"),
            ("1", "1"),
            ("2", "2"),
            ("3", "3"),
            ("4", "4"),
            ("5", "5"),
            ("6", "6"),
            ("7", "7"),
            ("8", "8"),
            ("9", "9"),
        ]
        clean_lines = []
        for line in lines:
            # Find first digit. lowest(idx, value)
            first = (99,0)
            for se in mapping:
                # breakpoint()
                idx = line.find(se[0])
                if idx != -1 and idx < first[0]:
                    first = (idx, se[1])
            last = (99, "0")  
            for se in mapping:
                r_line = line[::-1]
                # if se == ("nine", "9"):
                #     breakpoint()
                idx = r_line.find(se[0][::-1])
                if idx != -1 and idx < last[0]:
                    last = (idx, se[1]) 
            clean_lines.append(f"{first[1]}{last[1]}")
        extract = [re.findall("[0-9]", i) for i in clean_lines]
        result = 0
        for i in extract:
            result += int("".join([i[0],i[-1]]))
        return result


RiddleSolver(1).solve()
